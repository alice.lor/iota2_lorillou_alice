#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 14 15:20:24 2022

@author: alorillo
Program :iota2
CS GROUP
"""

import os
import glob


def calculateTime(path, typ):
    '''
    This function gives the sum of time's step (billing or walltime)

    Parameters
    ----------
    path : Str
        path for the log or the folder of one step for iota2
    typ : Str
        walltime or billing 

    Returns
    -------
    Str with the time in hour, minute, second

    '''
    list_folder =[]
    list_file=[]
    time =[]
    
    for element in os.listdir(path):
    	list_folder.append(element)
        
    #multiple steps
    if os.path.isfile(path+list_folder[0]):
       list_file.append(glob.glob(path+"*.out"))
       
    #only the time for one step
    else :
        list_file.append(glob.glob(path+"/*/*.out"))

    for i in range(len(list_file[0])):
        file = open(str(list_file[0][i]), "r")
        lignes = file.readlines ()
        for oneLign in lignes:
            field=oneLign.split()
            #We want billing
            
            if typ== 'billing': 
                if len(field)>0 and field[0] == 'BILLING':
                    time.append(field[2])

            #We want the walltime
            if typ=='walltime':
                if len(field)>0 and field[1] == 'USED':
                  walltime=field[3].split(',')
                  wall=walltime[5].split('=')
                  time.append(wall[1])
    second =0
    minut=0
    hour =0         
    for i in range(len(time)):
        valeur =time[i].split(':')
        second += int(valeur[2])
        minut += int(valeur[1])
        hour += int(valeur[0])

    if second > 60:
        minute = second // 60
        second =second % 60
        minut+= minute
    
    
    if minut > 60:
        heure = minut // 60
        minut =minut % 60
        hour+=heure 

    return('Temps : '+str(hour)+':'+str(minut)+':'+str(second))


if __name__ == "__main__":  

    path ='.../logs/'
    temps= calculateTime(path, 'walltime')
    print(temps)
