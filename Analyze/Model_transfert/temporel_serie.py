#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  5 14:17:02 2022

@author: alorillo
Program :iota2
CS GROUP
"""

import sqlite3
import pandas as pd
import matplotlib.pyplot as plt




def times_series(path, code, nomenclature, band, name, output ):
    '''
    

    Parameters
    ----------
    path : str
        path of the learning sample (SQlite)
    code : str
        code 
    nomenclature : str
        number of one class
    band : str
        name of the band / index 
    name : str
        name of the learning sample (SQlite) (table)
    output : str
       output path 

    Returns
    -------
    None.

    '''
    con1 = sqlite3.connect(path)


    dataframe =pd.read_sql_query('SELECT * from '+name+' where '+code+' ='+nomenclature,con1)
    std = dataframe.groupby(code).std()
    med = dataframe.groupby(code).median()
        
    
    primitive= [col for col in dataframe.columns if col.startswith(band)] 
    

    prim_std= std[primitive].T
    prim_med=med[primitive].T

    prim_std['primitive']=prim_std.index
    prim_med['primitive']=prim_med.index
 

    prim_med.plot(x='primitive', y=int(nomenclature), yerr=prim_std, use_index=False)
    plt.xticks([0, 16,  34], ['January',  'June',  'December'])
    plt.xlabel('Date 2018')
    plt.ylabel('NDVI (/1000)')
    plt.savefig(output, dpi=400) 
    plt.title('Times series of reflectance')
    plt.show()

  
 
      




if __name__ == "__main__": 

    path= '.../Archive/Output_iota2/LearningSamples_sqlite_code1/T30TXT_region_1_seed0_Samples_learn.sqlite'

    name='output'
    code='code2v1'
    nomenclature='323'
    band='sentinel2_ndvi'
    output='/home/alorillo/Documents/Graphiques/culture_hivers_code1.png'
    times_series(path,  code, nomenclature, band, name, output )
    
    
  
