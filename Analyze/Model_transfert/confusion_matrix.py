#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 11 10:39:50 2022

@author: alorillo
Program:   iota2 https://framagit.org/iota2-project/iota2/
CS GROUP

"""

import os
import sys
import shutil
import glob
from iota2.validation import results_utils as resU

import otbApplication



def gen_csv_matrix_list(path_imageClassification,output_path, code, path_validation_data):
    '''
    This function create the confusion matrix.


    Parameters
    ----------
    path_imageClassification : str
        path of classification (tif)
    output_path : str
        output path 
    code : str
         code
    path_validation_data : str
        path validation shapefile 

    Returns
    -------
    None.

    '''
    
    if os.path.isdir(output_path+'AllConfusionMatrix'):
        shutil.rmtree(output_path+'AllConfusionMatrix')
    os.mkdir(output_path+'AllConfusionMatrix')
    for i in range(len(path_validation_data)):
        name, _ = os.path.splitext(os.path.basename(path_validation_data[i]))
        ComputeConfusionMatrix = otbApplication.Registry.CreateApplication("ComputeConfusionMatrix")
        ComputeConfusionMatrix.SetParameterString("in", path_imageClassification)
        ComputeConfusionMatrix.SetParameterString("out",output_path+'AllConfusionMatrix/Confusion_Matrix_'+str(name)+'.csv')
        ComputeConfusionMatrix.SetParameterString("ref","vector")
        ComputeConfusionMatrix.SetParameterString("ref.vector.in",path_validation_data[i])
        ComputeConfusionMatrix.SetParameterString('ram', '1000')
        ComputeConfusionMatrix.SetParameterString("ref.vector.field", code)
        ComputeConfusionMatrix.ExecuteAndWriteOutput()
    

    
def gen_conf_matrix_(path,path_nom):
    '''
    This function generates a confusion matrix with the iota2 confusion matrix format.

    Parameters
    ----------
    path : str
        the output path with the csv of the confusion matrix
    path_nom : str
        the nomenclature

    Returns
    -------
    None.

    '''
    all_csv_in=[]
    for filename in glob.glob(path+"*.csv"):
        all_csv_in.append(filename)
    lab_conv_tab = None
    
    for seed_csv in all_csv_in:
        name, _ = os.path.splitext(os.path.basename(seed_csv))
        list_csv=[]
        list_csv.append(seed_csv)
        resU.stats_report(list_csv,
                          path_nom,
                          path+'Result_'+str(name)+'.txt',
                          labels_table=lab_conv_tab)
    
     
    for seed_csv in all_csv_in:
        name, _ = os.path.splitext(os.path.basename(seed_csv))
        out_png = os.path.join(path, f"{name}.png")
        resU.gen_confusion_matrix_fig(seed_csv,
                                      out_png,
                                      path_nom,
                                      undecidedlabel=None,
                                      dpi=200,
                                      write_conf_score=True,
                                      grid_conf=True,
                                      conf_score="count_sci",
                                      threshold=0.1,
                                      labels_table=lab_conv_tab)
        
        
    
        
if __name__ == "__main__":
    path ='.../Archive/Output_iota2/Validation/RF/RF_code0/Tiles_fusion/T30TXT/'
    path_nom='/home/alorillo/Images/nomenclature/nomenclature_code_2.txt'
    gen_conf_matrix_(path,path_nom)
    
