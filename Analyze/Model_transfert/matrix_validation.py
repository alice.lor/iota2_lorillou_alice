#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  9 11:43:08 2022

@author: alorillo
Program :iota2
CS GROUP
"""

import glob
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn



def matrix_validation_tile(liste, tile, algo, path, out_path):
    '''
    This function shows F1-scores by class (model learned on tile et model applied on tile) 


    Parameters
    ----------
    liste : list
        list with the name of the different classes
    tile : list
        list with the name of the tiles that correspond to the different folders
    algo : str
        name of the algorithm used
    path : str
        path of the folder (name of the tiles) which correspond to the tile learning
    out_path : str
        output path 

    Returns
    -------
    None.

    '''
    for name_class in range(len(liste)):
        matrix_f1score = np.zeros((len(tile),len(tile)))
        for folder_tile in range(len(tile)):
            for file_tile in range(len(tile)):
                for filename in glob.glob(path+"/"+tile[folder_tile]+"/*"+tile[file_tile]+"_seed_0_val.txt"):
                    f = open(filename, 'r')
                    for line in f:
                        columns = line.split('|')
                        columns = [col.strip() for col in columns]
                        if len(columns) == 5 and columns[0] == liste[name_class]:
                            matrix_f1score[folder_tile][file_tile] = columns[3]
        
        df_f1_score = pd.DataFrame(matrix_f1score, columns =tile, index = tile)
        figure, ((ax)) = plt.subplots(1, 1, figsize=(10,6))
        svm=seaborn.heatmap(df_f1_score,annot =True,cmap='RdYlGn',linewidths=0.30, vmin=0, vmax=1, ax=ax)
        ax.set_title('F1-score '+liste[name_class]+' ('+algo+')')
        ax.set_xlabel('Model learned on tile')
        ax.set_ylabel('Model applied on tile')
        figure = svm.get_figure()
        figure.savefig(out_path+'f1_score_matrice'+liste[name_class]+'.png',bbox_inches='tight', dpi=200)
        plt.show()
            
            
if __name__ == "__main__": 


    liste_code2=['proteagineux','cereale a paille','mais','tournesol','riz','soja','colza','tubercule racine','prairie','vergers','vignes','serre','batiment agricole','batiment commercial',
                 'batiment industriel','batiment indifferencie','batiment divers','chataigner','chene','hetre','robiner','peuplier','meleze','conifere','douglas','pin', 'sapin epicea','eau']       
    tile=['T30TXT','T31TCJ','T31TDN', 'T31TFJ','T31TFM','T31UDQ' ] 
    path='.../Archive/Output_iota2/Validation/RF_data_aug/RF_code2/Tiles_fusion/'  
    out_path='.../Archive/Output_iota2/Validation/RF_data_aug/RF_code2/F1_score/'
    
    fscore_more_close(liste_code2, tile, 'Random Forest Data Augmentation Code 2',path, out_path)
