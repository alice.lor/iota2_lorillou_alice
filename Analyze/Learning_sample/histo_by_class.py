#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 19 10:16:52 2022

@author: alorillo
Program:   iota2
CS GROUP
"""


import pandas as pd
import matplotlib.pyplot as plt


def histo_by_class(path,out):
    '''
    This function creates a histogram with the number of learning samples for each class.
    We need the script nbsample_by_class.py

    Parameters
    ----------
    path : str 
        path of the csv with the number of sample by class
    out : str
        the output path of the png

    Returns
    -------
    None.

    '''

    data = pd.read_csv(path, sep=',',header=0)
    X = list(data.iloc[:, 3])  
    Y = list(data.iloc[:, 2])
    plt.bar(X, Y, color='g')
    plt.title("Number of samples by class")
    plt.xlabel("Class")
    plt.ylabel("Number of samples")
    plt.xticks(rotation=90)
    plt.savefig(out, bbox_inches = 'tight', dpi=200)
    
   



                
if __name__ == "__main__": 
    path = '.../Archive/Output_iota2/Learning_sample/Code3/T30TXT_samples_region_1_seed_0_selection.csv"' 

    out='.../Archive/Output_iota2/Learning_sample/Code3/T30TXT_samples_region_1_seed_0_selection.png'

    histo_by_class(path,out)























