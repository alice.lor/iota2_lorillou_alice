#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 19 14:01:08 2022

@author: alorillo
Program:   iota2
CS GROUP
"""


import sqlite3
import pandas as pd
import numpy as np

def nbsample(path,sqlite,code, dictionnaire,output_path):
    '''
    This function gives the number of sample by class

    Parameters
    ----------
    path : str
        path of learning sample sqlite
    sqlite : str
        the name of the sqlite
    code :  str
        code (nomenclature)
    dictionnaire : dict
        dictionnaire for converting the number of class with the name of the class
    output_path : str
        the output path 

    Returns
    -------
    None.

    '''
    con = sqlite3.connect(path+sqlite+'.sqlite')
    data=pd.DataFrame()
    
    liste =pd.DataFrame({'Class' :list(dictionnaire.values())})
    data=data.append(liste)
    #Query with count group by code 
    dataframe = pd.read_sql_query('SELECT '+ code+', COUNT(*) FROM '+sqlite+' GROUP BY '+code, con)
    dataframe[code]=dataframe[code].astype(int)
    
    #for have the name of class
    dataframe['Class']=dataframe[code].replace(dictionnaire)
    df=data.set_index('Class').join(dataframe.set_index('Class'),  lsuffix='_caller', rsuffix='_other')
    df=df.replace (np.nan, 0)
    df['Class'] = df.index  
    df.to_csv(output_path)
    con.close()
              
if __name__ == "__main__":     

    path =".../learning_sample"
    sqlite='T30TXT_samples_region_1_seed_0_selection'
    output_path=".../Archive/Output_iota2/Learning_sample/Code3/T30TXT_samples_region_1_seed_0_selection.csv"
    code='code3v1'
    dictionnaire={1111:'Proteagineux', 1121:"Blé tendre d'hiver", 1122:"Orge d'hiver",1123: "Triticale d’hiver", 1124:"Orge de printemps",
                  1125:'Blé dur d’hiver', 1211:"Mais",1221:"Tournesol",1241:'Soja', 1231:'Riz', 1251:'Colza', 1261:'Tubercule et racine',
                  1311:'Prairie', 1321:'Vergers', 1331:'Vignes',2111:'Serre', 2121: 'Batîment agricole', 2211:'Batîment commercial', 2221:'Batiment industriel', 2311:'Batîment indifférencie',
                
                2324: 'Batîment sportif', 3121:'Chataigner', 3131: 'Chêne décidus', 3132:'Chêne serpiverent', 3141:'Hêtre', 3151:'Robinier', 3161:'Peuplier', 3251:'Mélèze',3211: 'Conifère', 
               3221:'Douglas', 3231:'Pin à crochet', 3232:"Pin d'Alep", 3233: 'Pin Maritime', 3234:'Pin laricro, pin noir', 3235:'Pin sylvestre', 3236:'Pin mélangé', 3237:'Pin autre',
               3241:'Sapin épicéa', 4111:'eau'}
    
    nbsample(path,sqlite,code, dictionnaire,output_path)

