#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 22 10:20:42 2022

@author: alorillo
Program :iota2
CS GROUP
"""

import pandas as pd
import matplotlib.pyplot as plt


def autopct(pct, min_percentage):
    return ('%4.1f%%' % pct) if pct > min_percentage else ''

def somme(frame, li,min_percentage):
    liste=[]

    for i in range(len(frame)):
        liste.append(frame[i]/frame.sum()*100)
        if liste[i]<min_percentage:
            li[i] =''
    return li
        


def pie(path, dictio, code,colors, title, output_path, min_percentage):
    '''
    This function shows the percentage of class classified (in nomenclature 3/2/1) labeled with the nomenclature 0

    Parameters
    ----------
    path : Str
       Path of the csv with the number of pixel attributed the nomenclature 0
    dictio : dict
        the dictionnaire {the code of class : the name of class}
    code : str
        code
    colors : list
        list of different colors we want for each class
    title : str
        the title of the figure
    output_path : str
         output path 
    min_percentage : int
        The minimum percentage by class we want to show in graphic

    Returns
    -------
    None.

    '''
    matrix_confu = pd.read_csv(path, index_col=0)
    matrix_confu=matrix_confu.T
    matrix_confu[code] = matrix_confu.index
    remplace=matrix_confu[code].replace(dictio)
    matrix_confu[code] = remplace.values
    class0=matrix_confu.columns.tolist()[0:4:]
    matrix_confu=matrix_confu.T
    fig = plt.figure(figsize=(5, 5))
    fig, ((ax1, ax2), (ax3,ax4)) = plt.subplots(2, 2, figsize=(18,15)) 
    fig.suptitle( fontsize=20, color="#444444")
    liste_ax=[ax1, ax2, ax3, ax4]
    width = .4

    for i in range(len(class0)):
        nw =somme(matrix_confu.loc[class0[i]], matrix_confu.iloc[4,:].tolist())
   
        liste_ax[i].pie(
            matrix_confu.loc[class0[i]],
            radius=1,
            colors=colors,
            wedgeprops={"width": width, "linewidth": 1},
            labels=nw,
            autopct=autopct,
            pctdistance=.87
            )
        liste_ax[i].set_title(class0[i], fontsize=18, color="#555555")
    
    plt.savefig(output_path,dpi=200, bbox_inches='tight', fontsize=40)



if __name__ == "__main__": 
    path= '.../Archive/Analyze/Pie/Confusion_matrix_code0.csv'
    min_percentage =1
    dictio={111: 'Protéagineux' , 112:'Céréale à paille' , 121: 'Mais', 122: 'Tournesol', 123: 'Riz',124:'Soja',
            125: 'Colza', 126: 'Tubercule et racine',131: 'Prairie', 132: 'Verger', 133 :'Vignes', 211 : 'Serre', 
            212: 'Batiment agricole', 221: 'Batiment commercial', 222: 'Batiment industriel', 231: 'Batiment indifferencie',
            232: 'Batiment divers', 312: 'Chataigner', 313: 'Chene', 314:'Hêtre', 315: 'Robiner', 316: 'Peuplier', 321:'Conifère' ,
            322: 'Douglas', 323:'Pin', 324: 'Sapin epicéa', 325: 'Melèze', 411: 'Eau'}
    
    
    colors_code2=['#f0c399', '#e5b778','#fedf00', '#ffab44', '#d5cfaa', '#e8fd27',  '#f9702c','#fb494c',
                  '#a8e79a', '#7deda8',  '#c087f8','#bc7ff8', '#7b0c65', '#7cd280', '#f04aff','#a4008b','#fe14bb', '#ff00cc',
                  '#a44e3b', '#02d369', '#57c832', '#5f8c58','#93b728', '#056717', '#369347', '#1c6822', '#05301e',  '#077808', '#0033b4']
    code='Code2'
    title="Percentage of validation pixel (Code 3) in class (CODE 0) \n for Random forest"
    output_path=".../Archive/Analyze/Pie/pie.png"
    pie(path, dictio, code,colors_code2, title, output_path, min_percentage)















