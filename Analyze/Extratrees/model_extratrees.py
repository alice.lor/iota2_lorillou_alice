#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 29 10:44:29 2022

@author: alorillo
Program:   iota2
CS GROUP
"""


import pickle as cp
import sqlite3
import pandas as pd
import matplotlib.pyplot as plt


def most_importance(path_learning, path_model,tile,prim, output):
    '''
     This function shows the importance of one primitive in extratrees model. 

    Parameters
    ----------
    path_learning : Str
        path with learning sample sqlite
    path_model : Str
        path with the extratrees model
    tile : list
        list of tiles 
    prim : str
        name of the primitive 
    output : str
        output path for the graphic

    Returns
    -------
    None.

    '''

    con = sqlite3.connect(path_learning)
    df_class = pd.read_sql_query('pragma table_info(output)', con )

    columns=df_class['name'].tolist()
    primitive=columns[7:len(columns)] 
    data = {'primitive': primitive}                

    for i in range(len(tile)):    
    
        model=cp.load(open(path_model+tile[i]+'/model_1_seed_0.txt', 'rb'))
        tree=model[0]
        importance=tree.feature_importances_.tolist()
        data[tile[i]]=importance

        
    prim='primitive'
    tile.insert(0,prim)
    dataframe= pd.DataFrame(data,columns=tile)
    dataframe=dataframe.set_index('primitive')
    dataframe= dataframe.T
    colonne=dataframe.columns
    data=dataframe[colonne]
    data['primitive']= data.index
    data.plot.scatter(y= prim, x='primitive', label=prim)
    plt.xticks(rotation=90) 
    plt.title('Importance values for primitives in classifications extratrees')
    plt.xlabel('Classification with different tile learning')
    plt.ylabel('Feature Importance')
    plt.savefig(output,dpi=200, bbox_inches='tight', fontsize=40)    
    
            
if __name__ == "__main__": 
    path_model='.../Archive/Extratrees/code1/'
    path_learning ='.../Archive/Output_iota2/LearningSamples_sqlite_code1/T31UDQ_region_1_seed0_Samples_learn.sqlite'
    tile=['T30TXT', 'T31TCJ', 'T31TDN', 'T31TFM',  'T31TFJ', 'T31UDQ']
    primitive= 'sentinel2_b3_20180103'
    output='.../Archive/Analyze/Extratrees/prim_plot.png'
    most_importance(path_learning, path_model, tile, primitive, output)
  

