#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  9 17:11:41 2022

@author: alorillo
Program :iota2
CSGROUP
For having the CSV with all value : file_csv.py
"""



import pandas as pd
import os



def choose_mode_comparaison(comparaison, path, threshold_min, threshold_max, mode):
    """
    This function realized the mode of the comparaison : 
        - 'comparaison' : comparaison between baseline and supervised
        - 'validation' : comparaison between validation and baseline
        - 'algo' :comparaison between baseline and supervised for each algorithm

    Parameters
    ----------
    comparaison : Str
        Mode of comparaision we want 
    path : Str
        Path of the csv file with Learning, Validation, Code, Algo , OA, Kappa and class
    threshold_min : int 
        values of minimal threshold of amplitude 
    threshold_max : int
        values of maximal threshold of amplitude 
   
    mode : str
        Mode : high, low, medium or alert
    -high : for having the biggest amplitudes ( superior to threshold_max) for OA, Kappa of one class
    -low  : for having the smallest amplitudes ( inferior to threshold_min) for OA, Kappa of one class
    -medium :  for having the medium amplitudes ( between the threshold_min and the threshold_max ) for OA, Kappa of one class
    -alert : for having the tab_alert ( number of times we have high, low and medium)
    

    Returns
    -------
    None.

    """
    dataframe= pd.read_csv(path)
    dirname = os.path.dirname(path)

    tile=dataframe['Validation'].unique().tolist()
    code=dataframe['Code'].unique().tolist()

    algo =dataframe['Algo'].unique().tolist()
   
   
    if comparaison == 'comparaison':
        comparaison_baseline_supervise(path,tile, threshold_min, threshold_max, algo, mode, code[0], dirname)
    if comparaison == 'validation':

        comparaison_baseline_validation(path,tile, threshold_min, threshold_max, algo, mode, code[0],dirname)
    if comparaison == 'algo':
        comparasion_baseline_supervise_algo_diff(path,tile, threshold_min, threshold_max, algo, mode, code[0],dirname)
        

    


        
        
def comparaison_baseline_supervise(path,tile, threshold_min, threshold_max, algo, mode, code, dirname):
    """
    This function is the comparaison between baseline and supervised

    Parameters
    ----------
    path : str
        path of the csv
    tile : list
        list of the diff tile 
    threshold_min : int 
        values of minimal threshold of amplitude 
    threshold_max : int
        values of maximal threshold of amplitude 
    algo : str
        the algo we want the comparaison
    mode :  Mode : high, low, medium or alert
        -high : for having the biggest amplitudes ( superior to threshold_max) for OA, Kappa of one class
        -low  : for having the smallest amplitudes ( inferior to threshold_min) for OA, Kappa of one class
        -medium :  for having the medium amplitudes ( between the threshold_min and the threshold_max ) for OA, Kappa of one class
        -alert : for having the tab_alert ( number of times we have high, low and medium)
    code : str
       level of the nomenclature

    Returns
    -------
    None.

    """
    #remove the output path if it exists
    for k in range(len(algo)):
        if os.path.exists(dirname+'/'+mode+'_'+code+'_'+algo[k]+"_comparaison.txt"):
        
            os.remove(dirname+'/'+mode+'_'+code+'_'+algo[k]+"_comparaison.txt") 
            
        dataframe= pd.read_csv(path, index_col='Validation' )
        dataframe['Validation'] = dataframe.index
        
        #Selection of the supervised and the baseline
        df_supervise_baseline =dataframe[((dataframe['Learning'] == dataframe['Validation'] ) | (dataframe['Learning'] == 'Fusion' ) ) & (dataframe['Algo'] == algo[k]) ]
     
        #Drop the columns with 'str' so : ["Learning", 'Code', 'Validation','Algo']
        df_supervise_baseline=df_supervise_baseline.drop(columns=["Learning", 'Code', 'Validation','Algo'])
        
        #Group by the same validation and apply this function "x:(x.max() - x.min())*100" for having the amplitude in percentage
        grouped = df_supervise_baseline.groupby(["Validation"]).apply(lambda x:(x.max() - x.min())*100)
        
        #The alert tab empty
        tab_alert=pd.DataFrame(columns=['Tile','low','medium','high'])
        
        
        #for each tile count the number of high, low, medium 
        
        for i in range(len(tile)):
            
            grouped_by_tile=grouped.iloc[i,:]
            
            if mode == 'high':
                high=grouped_by_tile[grouped_by_tile>threshold_max].round(2)
                file= open(dirname+'/'+mode+'_'+code+'_'+algo[k]+"_comparaison.txt", "a")
                file.write('Comparaison of the supervised and the baseline apply on ' +high.name+': \n') 
                file.close()
                high.to_csv(dirname+'/'+mode+'_'+code+'_'+algo[k]+"_comparaison.txt", sep=' ', mode='a')
                
       
            if mode == 'low':
                low=grouped_by_tile[threshold_min>grouped_by_tile].round(2)
                file= open(dirname+'/'+mode+'_'+code+'_'+algo[k]+"_comparaison.txt", "a")
                file.write('Comparaison of the supervised and the baseline apply on ' +low.name+': \n') 
                file.close()
                low.to_csv(dirname+'/'+mode+'_'+code+'_'+algo[k]+"_comparaison.txt", sep=',', mode='a')
                
            if mode == 'medium':
                medium=grouped_by_tile[(threshold_min<grouped_by_tile) & (grouped_by_tile<threshold_max)].round(2)
                file= open(dirname+'/'+mode+'_'+code+'_'+algo[k]+"_comparaison.txt", "a")
                file.write('Comparaison of the supervised and the baseline apply on ' +medium.name+': \n') 
                file.close()
                medium.to_csv(dirname+'/'+mode+'_'+code+'_'+algo[k]+"_comparaison.txt", sep=',', mode='a')
                
            if mode=='alert':
             
                high_count=grouped_by_tile[grouped_by_tile>threshold_max].count()
                medium_count=grouped_by_tile[(threshold_min<grouped_by_tile) & (grouped_by_tile<threshold_max)].count()
                low_count=grouped_by_tile[threshold_min>grouped_by_tile].count()
                tab_alert = tab_alert.append({'Tile':tile[i],'low':low_count, 'medium':medium_count, 'high':high_count}, ignore_index=True)
                tab_alert.to_csv(dirname+'/'+mode+'_'+code+'_'+algo[k]+"_comparaison.csv", sep=',')
             
        
    






def comparaison_baseline_validation(path,tile,  threshold_min, threshold_max, algo, mode, code, dirname):
    """
    This function is the comparaison between baseline and validation

    Parameters
    ----------
   path : str
       path of the csv
   tile : list
       list of the diff tile 
   threshold_min : int 
       values of minimal threshold of amplitude 
   threshold_max : int
       values of maximal threshold of amplitude 
   algo : str
       the algo we want the comparaison
   mode :  Mode : high, low, medium or alert
       -high : for having the biggest amplitudes ( superior to threshold_max) for OA, Kappa of one class
       -low  : for having the smallest amplitudes ( inferior to threshold_min) for OA, Kappa of one class
       -medium :  for having the medium amplitudes ( between the threshold_min and the threshold_max ) for OA, Kappa of one class
       -alert : for having the tab_alert ( number of times we have high, low and medium)
   code : str
      level of the nomenclature
    Returns
    -------
    None.

    """
    #remove the output path if it exists
    for k in range(len(algo)):
        if os.path.exists(dirname+'/'+mode+'_'+code+'_'+algo[k]+"_comparaison_validation.txt"):
            os.remove(dirname+'/'+mode+'_'+code+'_'+algo[k]+"_comparaison_validation.txt")
            
        dataframe= pd.read_csv(path, index_col='Validation' )
        dataframe['Validation'] = dataframe.index
        tab_alert=pd.DataFrame(columns=['Tile Learning', 'Tile Validation','low','medium','high'])
       
        
        
        
        for j in range(len(tile)-1):
            for i in range(len(tile)):
                
                #the validation (without the supervised) on one tile
                df_validation =dataframe[(dataframe['Learning'] != dataframe['Validation']) & (dataframe['Validation'] == tile[i]) & (dataframe['Learning'] != 'Fusion') & (dataframe['Algo'] == algo[k] ) ]
                #the baseline valided on the same tile
                df_baseline= dataframe[(dataframe['Learning'] == 'Fusion') & (dataframe['Validation'] == tile[i] ) & (dataframe['Algo'] == algo[k] ) ]
                
                #for having the Leaning columns 
                extracted_col = df_validation["Learning"].reset_index(drop=True)
     
                df_baseline=df_baseline.drop(columns=["Learning", 'Code', 'Validation','Algo'])
                df_validation=df_validation.drop(columns=["Learning", 'Code', 'Validation','Algo'])
               
                #calcul the amplitude between the baseline and all the validation on the same tile with different learning
                df_diff= abs(df_validation.subtract(df_baseline))*100
                df_diff=df_diff.reset_index(drop=True)
                
                #add the learning columns
                df_amplitude =df_diff.join(extracted_col, lsuffix='_caller', rsuffix='_other')
                df_amplitude=df_amplitude.set_index('Learning')
       
                df_ampl_by_tile=df_amplitude.iloc[j,:]
                
                if mode == 'high':

                    high=df_ampl_by_tile[df_ampl_by_tile>=threshold_max].round(2)
                    file= open(dirname+'/'+mode+'_'+code+'_'+algo[k]+"_comparaison_validation.txt", "a")
                    file.write('Comparaison of the learning on '+high.name+'  apply on '+tile[i]+ ' and the baseline apply on '+tile[i]+' : \n') 
                    file.close()
                    high.to_csv(dirname+'/'+mode+'_'+code+'_'+algo[k]+"_comparaison_validation.txt", sep=' ', header =False, mode='a')
                    
        
                if mode == 'low':
       
                    low=df_ampl_by_tile[threshold_min>=df_ampl_by_tile].round(2)
                    file= open(dirname+'/'+mode+'_'+code+'_'+algo[k]+"_comparaison_validation.txt", "a")
                    file.write('Comparaison of the learning on '+low.name+'  apply on '+tile[i]+ ' and the baseline apply on '+tile[i]+' : \n') 
                    file.close()
                    low.to_csv(dirname+'/'+mode+'_'+code+'_'+algo[k]+"_comparaison_validation.txt", sep=' ', header =False, mode='a')
                
                if mode == 'medium':
                    medium=df_ampl_by_tile[(threshold_min<df_ampl_by_tile) & (df_ampl_by_tile<threshold_max)].round(2)
                    file= open(dirname+'/'+mode+'_'+code+'_'+algo[k]+"_comparaison_validation.txt", "a")
                    file.write('Comparaison of the learning on '+medium.name+'  apply on '+tile[i]+ ' and the baseline apply on '+tile[i]+' : \n')
                    file.close()
                    medium.to_csv(dirname+'/'+mode+'_'+code+'_'+algo[k]+"_comparaison_validation.txt", sep=' ', header =False, mode='a')
                
                if mode == 'alert':
                    high=df_ampl_by_tile[df_ampl_by_tile>=threshold_max]
                    high_count=df_ampl_by_tile[df_ampl_by_tile>=threshold_max].count()
                    medium_count=df_ampl_by_tile[(threshold_min<df_ampl_by_tile) & (df_ampl_by_tile<threshold_max)].count()
                    low_count=df_ampl_by_tile[threshold_min>=df_ampl_by_tile].count()
                    tab_alert = tab_alert.append({'Tile Learning':high.name, 'Tile Validation':tile[i],'low':low_count, 'medium':medium_count, 'high':high_count}, ignore_index=True)   
                    tab_alert.to_csv(dirname+'/'+mode+'_'+code+'_'+algo[k]+"_comparaison_validation.csv", sep=',') 
        
    
   
          
          
def comparasion_baseline_supervise_algo_diff(path,tile, threshold_min, threshold_max, algo, mode, code, dirname):
    """
    This function is the comparaison between baseline and supervised for each algorithm

    path : str
        path of the csv
    tile : list
        list of the diff tile 
    threshold_min : int 
        values of minimal threshold of amplitude 
    threshold_max : int
        values of maximal threshold of amplitude 
    algo : str
        the algo we want the comparaison
    mode :  Mode : high, low, medium or alert
        -high : for having the biggest amplitudes ( superior to threshold_max) for OA, Kappa of one class
        -low  : for having the smallest amplitudes ( inferior to threshold_min) for OA, Kappa of one class
        -medium :  for having the medium amplitudes ( between the threshold_min and the threshold_max ) for OA, Kappa of one class
        -alert : for having the tab_alert ( number of times we have high, low and medium)
    code : str
       level of the nomenclature
     Returns
     -------
     None.


    """
    if os.path.exists(dirname+'/'+mode+'_'+code+"_comparaison_by_algo.txt"):
        os.remove(dirname+'/'+mode+'_'+code+"_comparaison_by_algo.txt")
    
    dataframe= pd.read_csv(path, index_col='Validation' )
     
    #for each algo 
    for j in range(len(algo)):
        
        dataframe['Validation'] = dataframe.index
        tab_alert=pd.DataFrame(columns=['Tile Learning', 'Tile Validation','low','medium','high'])
        df_supervise_baseline_algo = dataframe[((dataframe['Learning'] == dataframe['Validation'] ) | (dataframe['Learning'] == 'Fusion' )) & (dataframe['Algo'] == algo[j] ) ]
        df_supervise_baseline_algo=df_supervise_baseline_algo.drop(columns=["Learning", 'Code', 'Validation','Algo'])
        grouped = df_supervise_baseline_algo.groupby(["Validation"]).apply(lambda x:(x.max() - x.min())*100)
        
        
        tab_alert=pd.DataFrame(columns=['Tile','low','medium','high'])
        for i in range(len(tile)):
            dfl=grouped.iloc[i,:]
     
            if mode == 'high':
                high=dfl[dfl>=threshold_max].round(2)
                file= open(dirname+'/'+mode+'_'+code+"_comparaison_by_algo.txt", "a")
                file.write('Comparaison of the supervised on '+high.name+' and the baseline apply on ' +high.name+' with the algorithm '+algo[j]+': \n') 
                file.close()
                high.to_csv(dirname+'/'+mode+'_'+code+"_comparaison_by_algo.txt", sep=' ',header =False, mode='a')
            
            if mode == 'low':
                low=dfl[threshold_min>=dfl].round(2)
                file= open(dirname+'/'+mode+'_'+code+"_comparaison_by_algo.txt", "a")
                file.write('Comparaison of the supervised on '+low.name+' and the baseline apply on ' +low.name+' with the algorithm '+algo[j]+': \n') 
                file.close()
                low.to_csv(dirname+'/'+mode+'_'+code+"_comparaison_by_algo.txt", sep=' ',header =False, mode='a')
          
                
            if mode == 'medium':
                medium=dfl[(threshold_min<dfl) & (dfl<threshold_max)].round(2)
                file= open(dirname+'/'+mode+'_'+code+"_comparaison_by_algo.txt", "a")
                file.write('Comparaison of the supervised on '+medium.name+' and the baseline apply on ' +medium.name+' with the algorithm '+algo[j]+': \n') 
                file.close()
                medium.to_csv(dirname+'/'+mode+'_'+code+"_comparaison_by_algo.txt", sep=' ',header =False, mode='a')
                
            if mode=='alert':
            
                high_count=dfl[dfl>=threshold_max].count()
                medium_count=dfl[(threshold_min<dfl) & (dfl<threshold_max)].count()
                low_count=dfl[threshold_min>=dfl].count()
                
                tab_alert = tab_alert.append({'Tile':tile[i],'low':low_count, 'medium':medium_count, 'high':high_count}, ignore_index=True)
                
        if mode== 'alert':
            file= open(dirname+'/'+mode+'_'+code+"_comparaison_by_algo.txt", "a")
            file.write('Comparaison of the supervised and the baseline with the algorithm '+algo[j]+': \n') 
            file.close()
            tab_alert.to_csv(dirname+'/'+mode+'_'+code+"_comparaison_by_algo.txt", sep=' ', mode='a')
 


    
    
if __name__ == "__main__": 
    path='.../Archive/Tool_Alert/CSV/code1.csv'
    comparaison='algo'
    threshold_min=5
    threshold_max=10
    mode='high'
    choose_mode_comparaison(comparaison, path, threshold_min, threshold_max, mode)

