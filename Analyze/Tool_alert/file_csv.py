#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  9 17:12:17 2022

@author: alorillo
"""
import glob
import pandas as pd
import csv

def add_columns (path, code,liste):
    '''
    Create the csv with columns 

    Parameters
    ----------
    path : Str
        path of the new csv.
    code : Str
        Nomenclature of the code ex: Code3
    liste : list
        liste of the different class

    Returns
    -------
    columns : TYPE
        the 

    '''
    fichier = open(path+code+'.csv','w')
    columns =('Learning','Validation','Algo','Code','OA','Kappa')
    for i in range(len(liste)):
         columns=columns+ (liste[i],)  
    columns1=[columns]
    obj = csv.writer(fichier)

    for element in columns1:
        obj.writerow(element)
    fichier.close()
    return columns
    
def name_columns(code,liste):
    columns =('Learning','Validation','Algo','Code','OA','Kappa')
    for i in range(len(liste)):
         columns=columns+ (liste[i],)  
    return columns
    

def add_csv(code,tile, algo,liste, path, learning, path_result ):
    '''
    This function is used in the function add_row_by_tile

    Parameters
    ----------
    code : str
        Nomenclature of the code ex: Code3
    tile : list
        tile's list
    algo : str
        the name of the algortihm used
    liste : list
        list of classes
    path : Str
        Path of the 'Result.txt'
    learning : Str
        name of the tile with which we learned
    path_result : Str
        path of the csv

    Returns
    -------
    None.

    '''
    ob=pd.read_csv(path_result )
    ob1=ob
    for t in range(len(tile)):
        for filename in glob.glob(path+"*"+tile[t]+"_seed_0_val.txt"):
  
            ob1.loc[1,'Validation']=tile[t]
            ob1.loc[1,'Learning']=learning
            ob1.loc[1,'Algo']=algo
            ob1.loc[1,'Code']=code

            f = open(filename, 'r')
            for line in f:
                columns = line.split('|')
                columns = [col.strip() for col in columns]
                
                if columns[0].startswith('KAPPA') :
                    kappa=columns[0].split(':')
                    ob1.loc[1,'Kappa']=kappa[1]
            
                    
                if columns[0].startswith('OA') :
                    OA=columns[0].split(':')
                    ob1.loc[1,'OA']=OA[1]
                 
                for i in range(len(liste)):
                    if len(columns) == 5 and columns[0] == liste[i]:
                        f1_score_i= columns[3]
                        ob1.loc[1,liste[i]]=f1_score_i
               
        ob =pd.concat([ob, ob1])
    ob=ob.fillna(0.0)
    ob_without_duplicates = ob.drop_duplicates()
    ob_without_duplicates.to_csv(path_result, mode='a', header=False,index=False)

def add_row_by_tile(code,tile, algo,liste,path, path_result):
    '''
    This function add row by tiles 

    Parameters
    ----------
    code : Str
        the code used : code3
    tile : list
        list of tile
    algo : Str
        the name of the algorithm used
    liste : list
        list of classes
    path : str
        Path of the result (Result.txt)
    path_result : str
        path of the csv (code3.csv)

    Returns
    -------
    None.

    '''
    columns=name_columns(code,liste)
    for i in range(len(tile)):
        add_csv(code,columns,tile, algo,liste, path+tile[i]+'/', tile[i], path_result)
                    
           
def delete_duplicate(path_csv):
    '''
    This function drop the duplicated rows

    Parameters
    ----------
    path_csv : Str
        Path of the csv (code3.csv)

    Returns
    -------
    None.

    '''
    df=pd.read_csv(path_csv)  
    ob_without_duplicates = df.drop_duplicates()
    ob_without_duplicates.to_csv(path_csv,index=False)  

    
    
if __name__ == "__main__": 
    #Pour le code3
    liste=['proteagineux','ble tendre h','orge h','triticale h','orge printemps','ble dur h','mais','tournesol','riz','colza','soja',
    'tubercule racine','prairie','vergers','vignes','serre','batiment agricole','batiment commercial','batiment industriel','batiment indifferencie',
    'batiment sportif','chataigner','chene decidus','chene serpiverent','hetre','robiner','peuplier','meleze','conifere','douglas','pin cembrio',
      'pin alep','pin maritime','pin noir','pin sylvestre','pin melange','pin autre','sapin epicea','eau'] 
    tile=['T30TXT','T31TCJ','T31TDN', 'T31TFJ','T31TFM','T31UDQ' ,'Fusion'] 
    code='code2'
    algo='deep_learning'

    #for create a csv with the columns 
    columns_csv=add_columns(code, liste)
    
    
    path_result = '/home/alorillo/Images/Script_atomat/csv/'+code+'.csv'
    
    #For add new row thank of the results
    path ='/home/alorillo/Images/deep_learning_code3/Tiles_fusion/'
    add_row_by_tile(code,tile, algo,liste, path, path_result)
    
    #delete the row duplicate
    delete_duplicate('/home/alorillo/Images/Script_atomat/csv/'+code+'.csv')

   

    
    
