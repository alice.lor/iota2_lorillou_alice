#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  3 11:36:29 2022

@author: alorillo
"""


import os
import sys

def data_aug(path_learning, path_output, maxi, label):
    '''
    

    Parameters
    ----------
    path_learning : str
       path of the learning samples
    path_output : str
        path of the sqlite with the augmentation samples
    maxi : str
        number of samples 
    label : list of str
        label of class to rise 

    Returns
    -------
    None.

    '''
    for i in range(len(label)):
        cmd= 'otbcli_SampleAugmentation -in '+path_learning+' -field code2v1 -label ' +label[i]+' -samples '+maxi+' -out '+path_output+'_region_1_seed0_augmented_Samples_learn' +label[i]+'.sqlite -exclude i2label region code2v1 originfid tile_o -strategy smote -strategy.smote.neighbors 5'
        os.system(cmd)





if __name__ == "__main__":
    
    path_learning=sys.argv[0]
    path_output=sys.argv[1]
    tile = sys.argv[2]
    maxi = sys.argv[3]
    label=['111', '112' , '121', '122', '123', '124','125', '126', '131','132', '133' , '211', 
        '212', '221', '222', '231',
        '232', '312', '313', '314', '315', '316', '321',
        '322', '323', '324', '325', '411']
    data_aug(path_learning, path_output,tile, maxi)
