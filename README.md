# iota2_Lorillou_Alice

This repository contains the python scripts made during the internship. The scripts allow to create a database and analyze the results of iota2.

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://framagit.org/alice.lor/iota2_lorillou_alice.git
git branch -M main
git push -uf origin main
```

## Script

Folder Creation_bdd :
---------------------

Selection_of_tiles :
--------------------

- Percentage_class_tile.py

This script calculates the percentage of land cover by class for one tile. 
The output is a csv file with the different land cover by class.

-Concat_csv_one_tile.py

This script concats all the csv files ( Percentage_class_tile.py) with the land cover of one tile in one dataset (csv).
The output is a csv file. 

- Kmean_by_tile.py

This script makes a clustering by tile with the algortihm K-Mean (librairy Skicit Learn).
The cluster regroups tiles with the most similar land cover. 
The output is a csv file with the tile and the cluster’s number of the tile.
The output is also a shapefile with the geometry of tile and the cluster’s number of the tile. 

-Heatmap.py

This function creates a heatmap with the euclidian distance between two tiles. We need the dataset (csv) with all the land cover of tile. 
The output is a figure with the euclidian distance (heatmap).

Creation_database :
-------------------

-add_value_field.py

 This script adds/modifies a value in a field (shapefile/Sqlite).

-unique_values_field.py 

This script gives all unique values ​​of a field in a Sqlite.

-fusion_class.py 

This script merges two classes from a reference value of another field.

Results analysis
----------------

-nbsample_byclass.py 

This script calculates the number of learning samples by class.

-histo_by_class.py 

This script produces a histogram of the number of learning samples by class.

Tool_Alert :
------------

file_csv.py

This script creates a csv file with all the indicators obtained for the classifications

tools_alert.py

This script chooses a comparison mode: 'comparaison', 'validation', 'algo'

Extratrees
----------

model_extratrees.py

This script displays the most important primitives in the extratree models.

data_augmentation.py

This script is used to create synthetic samples.


Analysis with hierarchical nomenclature
---------------------------------------

-pie_repartion_in_code0.py

This script shows charts with the percentage of samples from nomenclature 3, chosen for its accuracy, distributed among the four classes of nomenclature 0.

Times of steps (iota2 chain)
----------------------------

-Time_step.py

This function gives the sum of the times (billing or walltime) of the iota2 steps.

