#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 30 14:42:19 2022

@author: alorillo
Program : iota2
CS GROUP
"""


import glob
from osgeo import ogr


def ValueField(path, typ, field):
    '''
    This function gives all unique values of one field
    
    Parameters
    ----------
    path : Str
        Path of the shapefile/Sqlite
    typ : Str
       Type of the file : 'SQlite' or 'ESRI Shapefile'
    field : Str
        The name of the field

    Returns
    -------
    list_value : list
        list of unique values in one field

    '''
    
    list_file=[]
    list_file.append(glob.glob(path))
    list_file=list_file[0]
    list_value=[]
   
    for i in range(len(list_file)):
    
        driver = ogr.GetDriverByName(typ)
        dataSource = driver.Open(list_file[i], 1) # 0 means read-only. 1 means writeable.
        layer = dataSource.GetLayer()
        
        for feature in layer:            
            value =  feature.GetField(field)
            if value not in list_value:
                list_value.append(value)
                
    return list_value
    
if __name__ == "__main__":
 
    
    path='.../myFile.sqlite'
    typ='SQlite'
    field='code3v1'
    ValueField(path, typ, field)
