#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 18 13:46:27 2022

@author: alorillo

https://framagit.org/iota2-project/iota2/
Program : iota2
CS GROUP
"""

import glob
from osgeo import ogr



def fusion_classe(path,dictionnaire,code,code_init):
    '''
    This function merges classes

    Parameters
    ----------
    path : str
        path of the shapefile
    dictionnaire : dict
        dict {old value 1 : new value1, old value 2 : new value1}
    code : str
        name of the field in which we want to merge two classes
    code_init : str
        the code with the old values 

    Returns
    -------
    None.

    '''
    
    list_file=[]
    list_file.append(glob.glob(path))
    list_file=list_file[0]

    for i in range(len(list_file)):
    
        driver = ogr.GetDriverByName('SQLite')

        dataSource = driver.Open(list_file[i], 1) # 0 means read-only. 1 means writeable.
   
        layer = dataSource.GetLayer()
        for feature in layer:            
            value =  feature.GetField(code_init)
            for cle,valeur in dictionnaire.items():
                if value == cle:
                    feature.SetField(code, valeur)
                    layer.SetFeature(feature)           

                
if __name__ == "__main__":  
              

    path ='/media/alorillo/TOSHIBA/LearningSamples_sqlite/LearningSamples_sqlite_code2/T30TXT_region_1_seed0_Samples_learn.sqlite'
    dictionaire={2323:2311,2325:2311,3171:3251}
    code= 'code3v1'
    code_init= 'i2label'
    fusion_classe(path,dictionaire,code,code_init)
