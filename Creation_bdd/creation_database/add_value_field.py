#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun  3 16:31:16 2022

@author: alorillo
Program :iota2
CSGROUP
https://framagit.org/iota2-project/iota2/
"""

import glob
from osgeo import ogr

def add_new_value(dictionnaire,path,typ,field_ref, field_name):
    '''
    This function adds/modifies a value in a field in function of a reference field
    
    Parameters
    ----------
    dictionnaire : dict
        dictionnaire {reference value : new value } 
    path : Str
        Path of the shapefile/Sqlite
    typ : Str
       SQlite or ESRI Shapefile
    field_name : Str
        name of the new field 
    field_ref : Str
        name of the reference field

    Returns
    -------
    None.

    '''

    list_file=[]
    list_file.append(glob.glob(path))
    list_file=list_file[0]
    
    for i in range(len(list_file)):
    
        driver = ogr.GetDriverByName(typ)

        dataSource = driver.Open(list_file[i], 1) 
   
        layer = dataSource.GetLayer()
        for feature in layer:            
            value =  feature.GetField(field_ref)
            for key, val in dictionnaire.items():
                if value == key:
                    feature.SetField(field_name, val)
                    layer.SetFeature(feature)
                    
                    
if __name__ == "__main__":
    
    path='.../myFile.sqlite'
    dictionnaire ={1111: 1, 1121: 2, 1122: 3, 1123: 4, 1124: 5, 1125: 6, 1211: 7, 1221: 8,
                   1231: 9, 1241: 10, 1251: 11, 1261: 12, 1311: 13, 1321: 14, 1331: 15, 2111: 16, 
                   2121: 17, 2211: 18, 2221: 19, 2311: 20, 2324: 21, 3121: 22, 3131: 23, 3132: 24, 
                   3141: 25, 3151: 26, 3161: 27, 3211: 28, 3221: 29, 3231: 30, 3232: 31, 3233: 32, 
                   3234: 33, 3235: 34, 3236: 35, 3237: 36, 3241: 37, 3251: 38, 4111: 39}
    typ='Sqlite'
    field_ref ='code3v1'
    field_name = 'i2label'
    
    add_new_value(dictionnaire,path,typ ,field_ref, field_name)
    
