#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 27 14:16:08 2022

@author: alorillo
Program :iota2
CSGROUP
"""

import geopandas


def percentage_by_class (paths_region, paths_ref_data, list_region, code, out_statistic):
    '''
    This function calculates the percentage by class (land cover) for one tile (in csv)
    

    Parameters
    ----------
    path_tile : Str
        Path of the shapefile of the tile // region eco-climatic
    path_ref_data : Str
        Path of the corine land cover or an other land cover database
    tile : Str
        name of tile we want to have the percentage (land cover)
    code : Str
        code of class for example 'CODE_18' in the database
    output : Str
        output path

    Returns
    ------
    None.

    '''
     
    #open the vector layer
    gdf_ref_data = geopandas.read_file(paths_ref_data)
    gdf_regions = geopandas.read_file(paths_region)
    region=gdf_regions.columns[0]
    
    
    #for the different regions // tiles 
   
    for i in range(len(list_region)):
        print(gdf_regions)
        gdf_region=gdf_regions[(gdf_regions[region] == list_region[i])] 
        gdf_region['arearegion']=gdf_region.area.sum()

        
            #Intersection tile and feature 
        intersection = gdf_region.overlay(gdf_ref_data, how='intersection')
        if intersection.empty: 
            print("No intersection between tile and data")
            
        else : 
            areatile= intersection['arearegion'][0]    
                #Calcul of the area
            intersection['area']=intersection.area
            
                #Calcul of the area percentage
            intersection['area_percent'] = (intersection['area']/ intersection['arearegion']*100)
                
                #Sum by the same code
            intersection_byclass=intersection.groupby(by=code, as_index=False).sum()
            
                #Change the int type by the string
            intersection_byclass[code] = intersection_byclass[code].astype(str)
            
                #save in csv
            intersection_byclass['area_percent']=intersection_byclass['area_percent'].round(2)
            intersection_byclass['area']=intersection_byclass['area'].round(2)
            intersection_byclass['arearegion']= areatile.round(2)
                
            intersection_byclass=intersection_byclass.rename(columns={'area_percent': list_region[i]})
            intersection_byclass=intersection_byclass.transpose() 
            intersection_byclass.to_csv(out_statistic+'out '+ list_region[i]+'.csv')
    
    

if __name__ == "__main__":
    #Datas :
        
    paths_ref_data='.../Archive/Selection_of_tiles/Datas/corine_land_cover/CLC18_FR.shp'
    paths_region =".../Archive/Selection_of_tiles/Datas/tile/S2_grid_V2.shp"
    out_statistic=r'.../Archive/Selection_of_tiles/Output/'
    region= ['T30TXT']
    code='CODE_18'
    
    
    
    percentage_by_class(paths_region,paths_ref_data, region, code, out_statistic)
