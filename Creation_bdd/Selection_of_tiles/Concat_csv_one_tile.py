#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 27 11:21:18 2022

@author: alorillo
Program :iota2
CSGROUP
"""

import pandas as pd
import os
import glob


def concat(code, path, out_path):
    """
    This function concats all the csv files.

    Parameters
    ----------
    code : code
        str
    path : path of the csv of each tile or region
        str
    out_path : path of the csv with the all percentage of class by tile 
       str

    Returns
    -------
    dataframe

    """
    list_folder =[]
    list_file=[]
    list_csv=[]


    for element in os.listdir(path):
        list_folder.append(element)
    for i in range(len(list_folder)):
        list_file.append(glob.glob(path+"*.csv"))
    for i in range(len(list_file)):    
        list_csv.append(pd.read_csv(list_file[0][i], header=1))
    
    #concat all dataframe for the different tile
    dataset_tile = pd.concat(list_csv) 
    dataset_tile.set_index(code, inplace = True)
    
    #change NaN value with 0 value 
    dataset_tile.fillna('0.0',inplace=True)
    dataset_tile.to_csv(out_path)
    
    return dataset_tile

if __name__ == "__main__":    

     path='.../Archive/Selection_of_tiles/Output/percentage_by_class_each_tile/'
     code = 'CODE_18'
     out_path= r'.../Archive/Selection_of_tiles/Output/percentage_by_class/dataset_tile.csv'
     dataset_tile=concat(code, path, out_path)   
