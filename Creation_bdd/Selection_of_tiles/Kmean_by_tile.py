#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 27 15:37:49 2022

@author: alorillo
Program :iota2
CSGROUP
"""


import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
import geopandas

def clustering (code, n_clusters, dataset_tile,path_tiles, out_path) :
    """
    This function makes a clustering by tile 
    The cluster regroups tiles with the most similar land cover
    

    Parameters
    ----------
    code : Str
        code
    n_clusters : int
        number of cluster
    dataset_tile : dataframe
        data with land cover percentage by tile 
    path_tiles : Str
        Path of shapefile with tiles 
    out_path : Str
        The output path of the shapefile and the csv 
        The index (field) is the number of the group

    Returns
    -------
    None

    """
    
    gdf_tiles = geopandas.read_file(path_tiles)
    
    #model of the Kmeans
    model=KMeans(n_clusters=n_clusters)
    #model apply on the dataset
    model.fit(dataset_tile)
    
    #index tried by group
    idk = np.argsort(model.labels_)

    #add the index
    tiles_cluster=pd.DataFrame(dataset_tile.index[idk],model.labels_[idk])
    tiles_cluster['index'] = tiles_cluster.index
    tiles_cluster.to_csv(out_path +'cluster'+str(n_clusters)+'.csv')
  
    df=gdf_tiles.join(tiles_cluster.set_index(code), on='tile')
    df = geopandas.GeoDataFrame(df, geometry='geometry')
    df.to_file(out_path+'tile_cluster_'+str(n_clusters)+'.shp', driver='ESRI Shapefile')
    
    
      

if __name__ == "__main__":    


     code = 'CODE_18'
     n_clusters=5
     dataset_tile=pd.read_csv('.../Archive/Selection_of_tiles/Datas/corine_land_cover/dataset_tile.csv', index_col=(code), header=0)
     path_tiles =".../Archive/Selection_of_tiles/Datas/tile/S2_grid_V2.shp"
     out_path='.../Archive/Selection_of_tiles/Output/Kmeans/'
     
     cluster=clustering (code, n_clusters, dataset_tile, path_tiles,out_path) 

     
