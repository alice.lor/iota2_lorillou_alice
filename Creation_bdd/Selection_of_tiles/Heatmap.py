#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 27 15:19:29 2022

@author: alorillo
Program :iota2
CSGROUP
"""

import pandas as pd
import numpy as np
import geopandas
from sklearn.metrics.pairwise import euclidean_distances
import matplotlib.pyplot as plt
import seaborn
from matplotlib import cm 


def heatmap( code, paths_tiles,  dataset_tile,out_path):
    """
    This function creates a heatmap with the euclidian distance between two tiles 

    Parameters
    ----------
    code : code
       str
    paths_tiles : path of the shapefile with tiles 
        str
    dataset_tile : dataframe who regroups all tiles (concat.py)
        dataframe
    out_path : output path
       str

    Returns
    -------
    heatmap.

    """

    gdf_tiles = geopandas.read_file(paths_tiles)
    
    #matrix for the euclidian distance 
    matrix = np.zeros([len(gdf_tiles),len(gdf_tiles)])
   
  
    
    for i in range(len(gdf_tiles)):
        for j in range(len(gdf_tiles)):
            tile1 = np.array([dataset_tile.loc[gdf_tiles['tile'][i]]])
            tile2 =  np.array([dataset_tile.loc[gdf_tiles['tile'][j]]])
            distance = euclidean_distances(tile1, tile2)
            matrix[i,j]= int(distance)
    
    liste_tiles =gdf_tiles.tile.tolist()
    
    
    df = pd.DataFrame(matrix, columns =liste_tiles, index = liste_tiles)  
    df.to_csv(out_path+'distance_tiles.csv')    
    plt.subplots(figsize=(40,25))
    initial_cmap = cm.get_cmap('RdYlGn')
    reversed_cmap=initial_cmap.reversed()
    plt.title('Euclidien distance of coverland area between two tiles' )
    svm=seaborn.heatmap(df, cmap =reversed_cmap, linewidths = 0.30, annot = True)
    figure = svm.get_figure()   
 
    figure.savefig(out_path+'heatmap.png', dpi=200)
    
    return svm



if __name__ == "__main__":    

  

     code = 'CODE_18'
     paths_tiles ="/media/alorillo/TOSHIBA/Archive/Selection_of_tiles/Datas/tile/S2_grid_V2.shp"
     out_path ='/media/alorillo/TOSHIBA/Archive/Selection_of_tiles/Output/heatmap/'
     dataset_tile=pd.read_csv('/media/alorillo/TOSHIBA/Archive/Selection_of_tiles/Output/percentage_by_class/dataset_tile.csv',index_col=('CODE_18'),  header=0)
     svm= heatmap( code, paths_tiles,  dataset_tile,out_path)
