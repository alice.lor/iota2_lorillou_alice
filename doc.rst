Impact of eco-climatic stratification on supervised classification algorithms for the production of large-scale land cover maps
*******************************************************************************************************************************

Reference database 
==================

Create a reference database
---------------------------


To perform a supervised classification with iota2, it is necessary to create a reference database that contains labeled features.
This reference database can be created manually with a geographic information system application such as Qgis.
With Qgis, creating a New Vector Dataset:

.. image:: image/create_feature.png
   :scale: 50 %
   :align: center

.. image:: image/layer.png
   :scale: 50 %
   :align: center


With insertion of the nomenclature in a corresponding field:

.. image:: image/labeled_feature.png
   :scale: 50 %
   :align: center

The second option is to use a database already existing for example:

1) BD TOPO : `https://geoservices.ign.fr/bdtopo`

.. image:: image/bd_topo.png
   :scale: 50 %
   :align: center

2) BD Fôret : `https://geoservices.ign.fr/bdforet`

.. image:: image/bd_foret.png
   :scale: 50 %
   :align: center

3) Corine Land Cover : `https://www.data.gouv.fr/fr/datasets/corine-land-cover-occupation-des-sols-en-france/`

.. image:: image/clc.png
   :scale: 50 %
   :align: center

4) RPG : `https://www.data.gouv.fr/fr/datasets/registre-parcellaire-graphique-rpg-contours-des-parcelles-et-ilots-culturaux-et-leur-groupe-de-cultures-majoritaire/`

.. image:: image/rpg.png
   :scale: 50 %
   :align: center



Insert a nomenclature in reference database 
-------------------------------------------

The database reference must have a field which contains the nomenclature.
If the database is created manually and have not many features, nomenclature can be inserted with a geographic information system (GIS) application like QGIS or ArcGis.

For existing databases the entity type names must be known : unique_values_field.py
If necessary, we can delete the feature with the values that one don't want:


.. code-block:: python

	for feature in layer:
		value = feature.GetField(code)
		if value == 'seigle':
			layer.DeleteFeature(feature.GetFID())


Then a field that will contain the code must be created : `https://framagit.org/iota2-project/iota2/-/blob/develop/iota2/vector_tools/add_field.py`

A dictionary must be created containing the initial values of the existing database and the label that will be assigned (Example :dict= {1: 'Culture', 2: 'Batiment', 3: 'Forêt', 4: 'Eau'}).
Then we add to the new field the label with `https://framagit.org/alice.lor/iota2_lorillou_alice/-/blob/main/Creation_bdd/creation_database/add_value_field.py`


Merge databases
---------------

If several databases are used, it is necessary to merge them. For this, the field's name with the nomenclature must be the same for all databases:
`https://framagit.org/iota2-project/iota2/-/blob/develop/iota2/vector_tools/merge_files.py`

Delete overlays in databases
----------------------------

If several databases have been merged, it is possible there are overlaps between the features. It's an issue if they don't have the same code.
Rasterize and polyonize allow to remove overlaps. Here are the two commands:

- otbcli_Rasterization -in databaseMerged.shp -out rasterImage.tif -spx 10 -spy 10
- gdal_polygonize.py rasterImage.tif databaseMerged_without_overlaps.shp


Fix training and validation databases
-------------------------------------

To compare the different algorithms between them, it can be important to fix the learning and validation databases. If learning samples and validation databases change between launching,the results are not really comparable.
The iota2 chain must first be launched. The learning samples are found in the folder learningSamples : *Samples_learn.sqlite* and in the validation database in the folder dataAppVal : *val.sqlite*.

iota2 only takes polygon shapefile layers as input, whereas the Samples_learn.sqlite file is points. A buffer can be applied to this layer with a geographic information system (GIS) application like QGIS or ArcGis.
For the validation database, we can have the results report with confusion_matrix.py which take as input the classification (.tif) and the database validation (.shp).
`https://framagit.org/alice.lor/iota2_lorillou_alice/-/blob/main/Analyze/Model_transfert/confusion_matrix.py`

Another issue is to merge some learning samples databases together. Sometimes, shapefile size can be superior to 2GB size and there is a 2GB size limit for any shapefile.
To solve this problem, it is possible to merge two sqlite databases, which is a file format that has no size limit.
This sqlite have to be insert in iota2 output with the name: Samples_region_1_seed0_learn.sqlite.
The chain can then be launched at step: 'Learn model' for algorithm: Random Forest or Extratrees.
The chain must be launched at the step: 'Merge samples dedicated to the same model' for Deep Learning.

Sampling strategy 
-----------------

Several sampling strategies can be used during iota2 launches. Sampling strategies reduce learning times if they are well-chosen. The first parameter is the ratio that separates the training and validation polygons.
The ratio parameter is by default 0.5. It is a suitable separation if the number of polygons is large. If the number of polygons is not large enough, it is better to increase the learning polygons.

The second sample parameter is the sample_selection.
The sampler can be periodic or random.
There are several strategies to select samples (parameter strategy):
- smallest: this strategy can be useful if you have a high minimum sample per class. It balances the classes.
- constant: it allows to having a random or periodic selection of the number of samples per class. This parameter was used during my internship, it is simple and preferable to the byclass strategy especially when there is many classes.
- byclass: this strategy is useful if there are few classes.
- percent: The aim of this strategy is to sample every polygon with a rate. If the rate is close to 1, the number of learning samples can be high. On a large scale, if the number of learning samples is too high, the chain may stop because it cannot sample too many points.


Sampling augmentation 
---------------------

Sometimes the number of learning samples by classes are unbalanced. Several strategies can be used to rebalance the classes.
SMOTE of OTB is a method that allows to recreate synthetic learning samples different from existing learning samples. First, SMOTE selects
randomly a learning sample in the learning database. Then the k
nearest neighbors are identified and one of the neighbors is randomly selected. A coefficient is randomly generated and the value of the new sample will be calculated between the value of the initial point and
the neighbor according to the coefficient. 

Command line SMOTE:

otbcli_SampleAugmentation -in samples.sqlite -field class -label 3 -samples 100 -out augmented_samples.sqlite -strategy smote -strategy.smote.neighbors 5

Supervised algorithms
=====================

To start, it is recommended to use the default algorithms settings in iota2. The parameters can be adjusted with tests.

Advantages and Disadvantages deducted from the internship:

**Random Forest** :

*Advantages*:	 
			 - robust to outliers  					  
			 - regression and classification 		
			 - learning time of 5 minuts  (one tile - 100 trees -150,000 samples )

*Disadvantages* : 
			 - complexity of analyzing large decision 
						

**Extratrees**

*Advantages* :
			 - learning time of 5 minuts (one tile - 100 trees -150,000 samples )

*Disadvantages* :		
			 - worse generalization than random forest  
							

**Multi Layer Perceptron**

*Advantages* :	
			 - good results on data close to the training data

*Disadvantages* :							  			
			 - worse generalization																
			 - architecture complex	
			 - learning time of 7 hours (one tile - 100 trees, 150,000 samples)





